<img src="https://i.imgur.com/Fw5tqN6.png"  width="150">

## Big data, comment ça marche?

Nicolas Parot Alvarez, Achitecte Data

[collectifconscience.org](https://collectifconscience.org)

---

### Définition du big data
Big data, ou mégadonnées, ou données massives désignent des ensembles de données trop **volumineuses**, **changeantes** ou **complexes** pour être traitées par des outils informatiques classiques.

---

<img src="https://i.imgur.com/8fpEIfX.png">

---

### Conséquences de cette définition
Le big data nécessite des nouveaux outils basés sur de (plus ou moins) nouveaux principes pour:
- collecter
- traiter
- stocker
- partager
- rechercher
- analyser
- visualiser
- protéger, garantir, mettre à jour, administrer...

---

### Brève histoire technologique du big data
- 1989, création "World Wide Web", Tim Berners-Lee, CERN
- 2003, publication "Google File System", Sanjay Ghemawat, Howard Gobioff, Shun-Tak Leung, Google
- 2004, publication "MapReduce", Jeffrey Dean, Sanjay Ghemawat, Google
- 2005, création de "Hadoop", Doug Cutting and Mike Cafarella, Yahoo!
- 2005, terme "big data", Roger Mougalas, directeur de recherches/architecte big data à O'Reilly

---

### Le concept fondamental: la distribution

<img src="https://i.imgur.com/mhcQDeM.png">

---

### Aplication technique 1: stockage distribué, 
Hadoop Distributed File System (HDFS)

<img src="https://i.imgur.com/umFysVW.png">

---

### Application technique 2: traitement distribué
MapReduce

<img src="https://i.imgur.com/vPyMSIs.png">

---

### La jungle du big data

<img src="https://i.imgur.com/VhPa556.png" width="800">

---

### La big data au service de la science

- Fondation Apache
- CERN (système de log)
- GIEC
- fold.it
- expériences scientifiques et missions spatiales
- datastro.eu

---

<img src="https://i.imgur.com/0TcJFYM.png">

---

<img src="https://i.imgur.com/F9KyVBO.png">

---

<img src="https://i.imgur.com/qLes2dr.png">


---

### L'apprentissage artificiel

Le big data est la fondation nécessaire à un apprentissage artificiel performant
<img src="https://i.imgur.com/bzs4QMn.png" width="800">


---

### Sources (à compléter)
- « Big Data – Mastère Spécialisé® de gestion et analyse des données massives » [archive], sur École d'Ingénieurs : Télécom ParisTech
- CEA, « Conférence : voyage au coeur du Big Data » [archive], sur CEA/Médiathèque, 5 juillet 2017
- Cukier, K., & Mayer-Schoenberger, V. (2013). Rise of Big Data: How it's Changing the Way We Think about the World
- Reinsel, David; Gantz, John; Rydning, John (13 April 2017). "Data Age 2025: The Evolution of Data to Life-Critical"
- https://www.cleverism.com/brief-history-big-data/
- https://www.datasciencecentral.com/profiles/blogs/who-came-up-with-the-name-big-data
- "The Google File System", 2003, https://ai.google/research/pubs/pub51
- "The Google File System", 2004, https://ai.google/research/pubs/pub62
- https://www.glassbeam.com/blog/impact-machine-data-analytics-artificial-intelligence-and-machine-learning-healthcare
- http://blog.inovia-conseil.fr/?p=46
- https://databricks.com/blog/2017/12/14/the-architecture-of-the-next-cern-accelerator-logging-service.html


---
Transformer en slides: https://gitpitch.com/collectif-conscience/eddl-data/master?grs=gitlab